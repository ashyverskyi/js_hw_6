// TASK 1

const product = {
    name: 'Apple Vision Pro',
    price: 160000,
    discount: 20010,

    discountMethod: function () {
        const discountedPrice = this.price - this.discount;
        return discountedPrice;
    }
};

console.log(`Final price including applicable discount is: uah${product.discountMethod()}`);

// TASK 2

function greetUser() {
    const userName = prompt("What is your name?");
    const userAge = prompt("How old are you?");

    if (userName && !isNaN(userAge)) {
        const greeting = `Hey there! I'm ${userAge} years old, and my name is ${userName}.`;
        alert(greeting);
    } else {
        alert("Check your inputs. You made a mistake.");
    }
}

greetUser();






